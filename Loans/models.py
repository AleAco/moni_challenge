from django.db import models

class Loan(models.Model):
    firstname=models.CharField(max_length=30)
    lastname=models.CharField(max_length=30)
    dni=models.IntegerField()
    gender=models.CharField(max_length=30)
    email=models.EmailField()
    amount=models.IntegerField()

    def __str__(self):
        return self.dni
