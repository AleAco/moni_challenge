from django.contrib import admin
from django.urls import path, include
from Loans.views import formLoan, login, getListLoans, modifyLoan, deleteLoan
from Loans.forms import FormAdmin

urlpatterns = [
    path('', formLoan, name="form"),
    path('list-loans/', getListLoans, name="list-loans"),
    path('modify-loan/<id>/', modifyLoan, name="modify-loan"),
    path('delete-loan/<id>/', deleteLoan, name="delete-loan"),
    path('accounts/login/', login, name="login"),
    path('accounts/', include('django.contrib.auth.urls')),
]
