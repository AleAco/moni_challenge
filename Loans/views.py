from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from Loans.forms import FormLoan, FormAdmin
from Loans.models import Loan
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.core.paginator import Paginator
from django.http import Http404
import os
import requests

def formLoan(request):
    if request.method == 'POST':
        formLoan = FormLoan(data = request.POST)

        if formLoan.is_valid():
            headers = {}
            MONI_CREDENTIAL = os.getenv('MONI_CREDENTIAL')

            if MONI_CREDENTIAL is not None:
                headers['credential'] = MONI_CREDENTIAL
            moni_api = 'https://api.moni.com.ar/api/v4/scoring/pre-score/{}'
            dni = request.POST.get("dni")
            result = requests.get(moni_api.format(dni), headers = headers)
            if result.status_code == 200:
                data = result.json()
                if 'status' in data:
                    if data['status'] == 'approve':
                        save = Loan.objects.create(
                            firstname = request.POST.get("firstname"),
                            lastname = request.POST.get("lastname"),
                            dni = request.POST.get("dni"),
                            gender = request.POST.get("gender"),
                            email = request.POST.get("email"),
                            amount = request.POST.get("amount")
                        )
                        messages.success(request, "El prestamo ha sido aprobado")
                        formLoan=FormLoan()
                    else:
                        messages.error(request, 'El prestamo para dni {} no se encuentra aprobado'.format(dni))
                        formLoan=FormLoan()
                else:
                    messages.error(request, 'No se pudo procesar el prestamo.')
            else:
                messages.error(request, 'La pagina no se encuentra o sus credenciales son invalidas.')
        else:
            messages.error(request, 'El formulario contiene errores, revise.')
            formLoan=FormLoan()
    else:
        formLoan=FormLoan()

    return render(request, 'form-loan.html', {"form": formLoan})


def login(request):
    if request.method == 'POST':
        form = FormAdmin(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)
        if user is not None:
            django_login(request, user)
            return redirect(to = "list-loans")
        else:
            messages.error(request, 'El usuario o la contraseña es incorrecto.')
    return render(request, 'registration/login.html', {"form": FormAdmin})

def getListLoans(request):
    if not request.user.is_authenticated:
        return redirect(to = "login")
    loans = Loan.objects.all()
    page = request.GET.get('page', 1)

    try:
        paginator = Paginator(loans, 5)
        loans = paginator.page(page)
    except:
        raise Http404

    return render(request, 'admin/list-loans.html', {"loans": loans, "paginator": paginator})

def modifyLoan(request, id):
    if not request.user.is_authenticated:
        return redirect(to = "login")
    loan=get_object_or_404(Loan, id = id)

    if request.method == 'POST':
        formLoan=FormLoan(data = request.POST, instance = loan)
        if formLoan.is_valid():
            formLoan.save()
            messages.success(request, "El pedido ha sido editado correctamente")
            return redirect(to = "list-loans")
    else:
        formLoan = FormLoan(instance = loan)

    data = {
        'form': formLoan
    }
    return render(request, 'admin/modify-loan.html', data)

def deleteLoan(request, id):
    if not request.user.is_authenticated:
        return redirect(to = "login")
    loan = get_object_or_404(Loan, id = id)
    loan.delete()
    messages.success(request, "El pedido ha sido eliminado correctamente")
    return redirect(to = "list-loans")
