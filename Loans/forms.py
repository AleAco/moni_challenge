from django import forms
from .models import Loan
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class FormLoan(forms.ModelForm):
    firstname=forms.CharField(label="Nombre:", max_length=30, required=True)
    lastname=forms.CharField(label="Apellido:", max_length=30, required=True)
    dni=forms.CharField(label="Dni:", min_length=8, max_length=12, required=True)
    gender=forms.CharField(label="Género:", max_length=30, required=True)
    email=forms.EmailField(label="Correo:", required=True)
    amount=forms.IntegerField(label="Cantidad:", required=True)

    class Meta:
        model = Loan
        fields = '__all__'
        help_texts = { k:"" for k in fields }

class FormAdmin(forms.Form):
    username=forms.CharField(label="Usuario:", required=True)
    password=forms.CharField(label="Contraseña", max_length=30, required=True, widget=forms.PasswordInput)
 
    class Meta:
        model = User
        fields = ['username', 'password']
        help_texts = { k:"" for k in fields }
